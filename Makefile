kdbtree: utility.o  kdbtree.o main.o
	nvcc -o kdbtree  utility.o kdbtree.o  main.o
main.o:main.cu
	nvcc -o main.o -c -g -G -arch=sm_20 main.cu
kdbtree.o:kdbtree.h kdbtree.c
	g++ -o kdbtree.o -c -g kdbtree.c
utility.o:utility.c
	g++ -o utility.o -c -g utility.c
clean:
	rm -rf *.txt *.o kdbtree
