
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "kdbtree.h"
#include "utility.h"

int kdb_search(struct kdbtree *tree, double x,double y);
void kdb_printf(struct kdbtree *tree,int first);
                                                                                        
static void insert_point(struct kdbPointPage *pointPage, struct kdbPoint *point);

static int insert_regionTuple(
		struct kdbRegionPage *rp, const struct kdbRegion *region, int page_type, int page_ID);

int insert_pointPage_in_global_list(struct kdbtree *tree,struct kdbPointPage **pointPage);

int insert_regionPage_in_global_list(struct kdbtree *tree,struct kdbRegionPage **rp);

void delete_regionTuple(struct kdbRegionPage *rp, int page_type, int page_ID);
int delete_pointPage_in_global_list(struct kdbtree *tree,struct kdbPointPage *pp);
int delete_regionPage_in_global_list(struct kdbtree *tree,struct kdbRegionPage *rp);

static void do_split_pointPage
(
	        struct kdbPointPage *oldPP,
		struct kdbPointPage *leftPP, 
		struct kdbPointPage *rightPP,
		double xi,
		int dir
);

static void do_split_regionPage
(
               struct kdbtree *tree,
		struct kdbRegionPage *oldRP, 
		struct kdbRegionPage *leftRegionPage, 
		struct kdbRegionPage *rightRegionPage,
		double xi,
		int dir
			
);
static int is_left_of_xi(const struct kdbRegion *region, int dir, double median);
static int is_right_of_xi(const struct kdbRegion *region, int dir, double median);
static void set_page_region
(
		const struct kdbRegion oldRegion,
		int dir,
		struct kdbRegion *newRegion,
		double splitElement,
		int side
);

static void reset_parent_id(struct kdbtree *tree,int child_page_type, int child_page_id, int new_parent_page_id);

int pointInRegion(const struct kdbRegion *region, const struct kdbPoint *point);
int pointInPointPage(const struct kdbPointPage *pointPage, const struct kdbPoint *point);
int pointsEqual(const struct kdbPoint *firstPoint, const struct kdbPoint *secPoint);

double find_splitting_element(const struct kdbPointPage *pointPage);
double find_splitting_element_in_RP( struct kdbRegionPage *regionPage);

static struct kdbPointPage *find_PointPage_contain_queryPoint(struct kdbtree *tree,const struct kdbPoint *queryPoint);

static void handle_pp_overflow(struct kdbtree *tree,struct kdbPointPage *pointPage);
static void handle_parentPage_of_pp
(
                struct kdbtree *tree,
		struct kdbRegionPage *pParent, 
		const struct kdbPointPage *old_PP, 
		struct kdbPointPage *new_leftPP, 
		struct kdbPointPage *new_rightPP
			
);

static void handle_rp_overflow(struct kdbtree *tree,struct kdbRegionPage *rp);

static void handle_parentPage_of_rp
(
                struct kdbtree *tree,
		struct kdbRegionPage *pParent, 
		struct kdbRegionPage *oldrp, 
		struct kdbRegionPage *new_leftRP, 
		struct kdbRegionPage *new_rightRP
			
);



struct kdbtree *kdb_create()                                                       
{
	struct kdbtree *tree;

	int i;

	if( ( tree = (struct kdbtree *)malloc( sizeof(struct kdbtree) ) ) == NULL ) 
	{
	  return NULL; 
	}

	tree->dim = DIMENSION;
	tree->root_page_type = UNKNOWN_PAGE;                      
	tree->isEmpty = 1; 
    tree->root_pageID = -1;
    tree->point_page_num = 0;
    tree->region_page_num = 0;

	for(i = 0; i < DIMENSION; i++) {
		tree->globalRegion.min[i] = RECTANGLE_MIN;                       
		tree->globalRegion.max[i] = RECTANGLE_MAX;                        
	}
        memset(tree->allPointPages,0,MAX_POINT_PAGE_NUM * sizeof(struct kdbPointPage) );
	memset(tree->allRegionPages,0,MAX_REGION_PAGE_NUM * sizeof(struct kdbRegionPage) );

	return tree;		
}

int kdb_insert(struct kdbtree *tree, struct kdbPoint *point)
{	

	struct kdbPointPage *pointPage = NULL;                         
	struct kdbPointPage *findedPP = NULL;
	
	if(tree->isEmpty) 
	{                                                        
                                              
        insert_pointPage_in_global_list(tree,&pointPage);	

		pointPage->dir = 0;
		pointPage->isRoot = 1; 
		pointPage->parPageID = -1;
                pointPage->pp_region = tree->globalRegion; 

		tree->isEmpty = 0;
		tree->root_page_type = POINT_PAGE;                                  
		tree->root_pageID = pointPage->pageID;                                     

		insert_point(pointPage, point);
                                     
		return INSERT_SUCC;	
	} 

	findedPP = find_PointPage_contain_queryPoint(tree,point); 	                  
	if (findedPP) 
	{
	
		if (pointInPointPage(findedPP, point)) 
		{		                         
			return 0;		
		} 
		else 
		{
			insert_point(findedPP, point);			
			if (findedPP->point_num <= MAX_POINT_NUM) 
			{
				return INSERT_SUCC;
			}
			else{
 			 handle_pp_overflow(tree,findedPP); 
			}
		 }
	} 
	else 
	{	
	  return INSERT_FAILED;
	}
	return 0;
}


static void handle_pp_overflow(struct kdbtree *tree,struct kdbPointPage *pointPage)
{
	int result;
	double xi;

	struct kdbRegionPage *rp = NULL;
	struct kdbPointPage * leftPointPage=NULL;
	struct kdbPointPage * rightPointPage=NULL;


	xi = find_splitting_element(pointPage); 

	insert_pointPage_in_global_list(tree,&leftPointPage);
	insert_pointPage_in_global_list(tree,&rightPointPage);

	do_split_pointPage(pointPage,leftPointPage,rightPointPage,xi,pointPage->dir);


	if (pointPage->isRoot) {

        insert_regionPage_in_global_list(tree,&rp);

		rp->parPageID = -1;

		rp->isRoot = IS_ROOT;

		rp->cur_region = pointPage->pp_region;

		rp->dir = 0;

		rp->nRegion = 0;

		leftPointPage->parPageID = rp->pageID;                 

		rightPointPage->parPageID = rp->pageID;

		result = insert_regionTuple(rp, &(leftPointPage->pp_region), POINT_PAGE,leftPointPage->pageID);

		result = insert_regionTuple(rp, &(rightPointPage->pp_region), POINT_PAGE,rightPointPage->pageID);    

		tree->root_page_type = REGION_PAGE;
		tree->root_pageID = rp->pageID;				
	} 
	else 
	{
		struct kdbRegionPage *pParent = &tree->allRegionPages[pointPage->parPageID];
		if(pParent==NULL)
		{
			printf("pointPage parent error\n");
			printf("pointPage->parPageID=%d\n",pointPage->parPageID);
			printf("pointPage->pageID=%d\n",pointPage->pageID);
			kdb_printf(tree,0);
			exit(1);
		}
  
		handle_parentPage_of_pp(tree,pParent,pointPage,leftPointPage, rightPointPage  );		
	}

	delete_pointPage_in_global_list(tree,pointPage);
	return;
}

static void handle_parentPage_of_pp(
struct kdbtree *tree,
struct kdbRegionPage *pParent, 
const struct kdbPointPage *old_PP,
struct kdbPointPage *new_leftPP, 
struct kdbPointPage *new_rightPP
) 
{
	int result;

	delete_regionTuple(pParent,POINT_PAGE,old_PP->pageID);

	insert_regionTuple(pParent, &(new_leftPP->pp_region), POINT_PAGE, new_leftPP->pageID);

	result = insert_regionTuple(pParent, &(new_rightPP->pp_region), POINT_PAGE, new_rightPP->pageID);


	if (result == IS_NOT_OVERFLOW)
	{
		new_leftPP->parPageID = pParent->pageID;
		new_rightPP->parPageID = pParent->pageID;
		return;
	}
	else{
		handle_rp_overflow(tree,pParent);
		return;
	}
}

static void handle_rp_overflow(struct kdbtree *tree,struct kdbRegionPage *rp)
{
	double xi;
	int result;

	struct kdbRegionPage *root_RP = NULL;
	struct kdbRegionPage *leftRegionPage=NULL;
	struct kdbRegionPage *rightRegionPage=NULL;


	insert_regionPage_in_global_list(tree,&leftRegionPage);
	insert_regionPage_in_global_list(tree,&rightRegionPage);

	xi = find_splitting_element_in_RP(rp);
	while((int)xi == 10)
	{
	  rp->dir = rp->dir + 1;
	  xi = find_splitting_element_in_RP(rp);
	}

	do_split_regionPage(tree,rp,leftRegionPage,rightRegionPage, xi,rp->dir);


	if (rp->isRoot) {

        insert_regionPage_in_global_list(tree,&root_RP);
		
		root_RP->parPageID = -1;

		root_RP->isRoot = IS_ROOT;

		root_RP->cur_region = rp->cur_region;

		root_RP->dir = 0;

		root_RP->nRegion = 0;

		leftRegionPage->parPageID = root_RP->pageID;

		rightRegionPage->parPageID = root_RP->pageID;

		result = insert_regionTuple(root_RP, &(leftRegionPage->cur_region), REGION_PAGE, leftRegionPage->pageID);

		result = insert_regionTuple(root_RP, &(rightRegionPage->cur_region), REGION_PAGE, rightRegionPage->pageID);

		tree->root_page_type = REGION_PAGE;

		tree->root_pageID = root_RP->pageID;

	} 
	else 
	{
		struct kdbRegionPage *pParent = &tree->allRegionPages[rp->parPageID];
		if(pParent==NULL)
		{
			printf("rp parent error\n");
			kdb_printf(tree,0);
			exit(1); 
		}
		handle_parentPage_of_rp(tree,pParent,rp,leftRegionPage,rightRegionPage);		
	} 

	delete_regionPage_in_global_list(tree,rp);
	return;
}


static void handle_parentPage_of_rp(
struct kdbtree *tree,
struct kdbRegionPage *pParent, 
struct kdbRegionPage *oldRP, 
struct kdbRegionPage *new_leftRP, 
struct kdbRegionPage *new_rightRP
) 
{

	int result;	

	delete_regionTuple(pParent, REGION_PAGE, oldRP->pageID);

	insert_regionTuple(pParent, &new_leftRP->cur_region, REGION_PAGE, new_leftRP->pageID);

	result = insert_regionTuple(pParent, &new_rightRP->cur_region, REGION_PAGE, new_rightRP->pageID);

	if (result == IS_NOT_OVERFLOW)
	{
		new_leftRP->parPageID = pParent->pageID;
		new_rightRP->parPageID = pParent->pageID;
		return;
	}

	handle_rp_overflow(tree,pParent);			
}

static void do_split_pointPage
(
struct kdbPointPage *oldPP,
struct kdbPointPage *leftPP, 
struct kdbPointPage *rightPP, 
	double xi,
	int dir
)
{

	double median;
	int i;

	median = xi;

	set_page_region(oldPP->pp_region, dir,&(leftPP->pp_region), median, LEFT);
	set_page_region(oldPP->pp_region, dir,&(rightPP->pp_region), median, RIGHT);             

	leftPP->dir = (dir + 1) % DIMENSION;
	rightPP->dir = (dir + 1) % DIMENSION;
    
	leftPP->parPageID = oldPP->parPageID;
	rightPP->parPageID = oldPP->parPageID;

	leftPP->isRoot=0;
	rightPP->isRoot=0;

    leftPP->point_num=0;
	rightPP->point_num=0;

    for(i=0;i<oldPP->point_num;i++)
		if (oldPP->pList[i].data[dir] < median) {
			insert_point( leftPP, &(oldPP->pList[i]) );		
		} else {		
			insert_point( rightPP,&(oldPP->pList[i]) );
		}

	if(leftPP->point_num == 0 || rightPP->point_num == 0)	
	{
	 //printf("error in split point page\n");
	 //printf("old_RP->point_num=%d\n",oldPP->point_num);
	 //printf("oldPP->pp_region min=(%f,%f) max=(%f,%f)\n",oldPP->pp_region.min[0],oldPP->pp_region.min[1],oldPP->pp_region.max[0],oldPP->pp_region.max[1]);
	 //printf( "leftPP->point_num=%d\n",leftPP->point_num);
	 //printf("leftPP->pp_region min=(%f,%f) max=(%f,%f)\n",leftPP->pp_region.min[0],leftPP->pp_region.min[1],leftPP->pp_region.max[0],leftPP->pp_region.max[1]);
	 //printf( "rightPP->point_num=%d\n",rightPP->point_num);
	 //printf("rightPP->pp_egion min=(%f,%f) max=(%f,%f)\n",rightPP->pp_region.min[0],rightPP->pp_region.min[1],rightPP->pp_region.max[0],rightPP->pp_region.max[1]);
	 //printf("median=%f\n",median);
	 //printf("dim=%d\n",dir);
     printf("there are empty point page\n");
	}
	return;		
}

void do_split_regionPage(
struct kdbtree *tree,
struct kdbRegionPage *old_RP, 
struct kdbRegionPage *leftRegionPage, 
struct kdbRegionPage *rightRegionPage,
	double xi,
	int dir
) 
{	

	double median;
	struct kdbRegionTuple *rt = NULL;
	int i;
	int result;


	struct kdbPointPage *child_pp = NULL;
	struct kdbRegionPage *child_rp = NULL;

	struct kdbPointPage *leftPointPage = NULL;
	struct kdbPointPage *rightPointPage = NULL;

	struct kdbRegionPage *childleftRegionPage = NULL;
	struct kdbRegionPage *childrightRegionPage = NULL;

	struct kdbRegionTuple *newRT = NULL;

	median = xi;


	leftRegionPage->parPageID = old_RP->parPageID;
	rightRegionPage->parPageID = old_RP->parPageID;

	set_page_region(old_RP->cur_region, dir, &(leftRegionPage->cur_region), median, LEFT);
	set_page_region(old_RP->cur_region, dir, &(rightRegionPage->cur_region), median, RIGHT);

	leftRegionPage->dir = (dir + 1) % DIMENSION;
	rightRegionPage->dir = (dir + 1) % DIMENSION;


	leftRegionPage->isRoot = 0;
	rightRegionPage->isRoot = 0;

	leftRegionPage->nRegion = 0;
	rightRegionPage->nRegion = 0;

	for(i = 0; i < old_RP->nRegion; i++) 
	{
		rt = &(old_RP->regionSet[i]);

		if ( is_left_of_xi(&rt->region, dir, median) )
		{
			insert_regionTuple(leftRegionPage, &rt->region, rt->pagetype, rt->pageID);
			reset_parent_id(tree,rt->pagetype, rt->pageID, leftRegionPage->pageID);		
		} 
		else if( is_right_of_xi(&rt->region, dir, median) ) 
		{		
			insert_regionTuple(rightRegionPage, &rt->region, rt->pagetype, rt->pageID);			
			reset_parent_id(tree,rt->pagetype, rt->pageID, rightRegionPage->pageID);
		} 
		else 
		{

			if (rt->pagetype == POINT_PAGE) 
			{
				child_pp = &tree->allPointPages[rt->pageID];
      

				insert_pointPage_in_global_list(tree,&leftPointPage);
				insert_pointPage_in_global_list(tree,&rightPointPage);

				do_split_pointPage(child_pp, leftPointPage, rightPointPage, median ,dir);

				result = insert_regionTuple(leftRegionPage, &(leftPointPage->pp_region), POINT_PAGE, leftPointPage->pageID);
				result = insert_regionTuple(rightRegionPage, &(rightPointPage->pp_region), POINT_PAGE,rightPointPage->pageID);


				if (result == IS_NOT_OVERFLOW)
				{
					leftPointPage->parPageID = leftRegionPage->pageID;
					rightPointPage->parPageID = rightRegionPage->pageID;
				}

				delete_pointPage_in_global_list(tree,child_pp);
			} 
			else if (rt->pagetype == REGION_PAGE) 
			{
				child_rp = &tree->allRegionPages[rt->pageID];
        

	     		insert_regionPage_in_global_list(tree,&childleftRegionPage);
				insert_regionPage_in_global_list(tree,&childrightRegionPage);

				do_split_regionPage(tree,child_rp, childleftRegionPage, childrightRegionPage,median,dir);

				result = insert_regionTuple(leftRegionPage,& (childleftRegionPage->cur_region), REGION_PAGE,childleftRegionPage->pageID);
				result = insert_regionTuple(rightRegionPage,&(childrightRegionPage->cur_region), REGION_PAGE,childrightRegionPage->pageID);

				if (result == IS_NOT_OVERFLOW)
				{
					childleftRegionPage->parPageID = leftRegionPage->pageID;
					childrightRegionPage->parPageID = rightRegionPage->pageID;
				}
				delete_regionPage_in_global_list(tree,child_rp);
			} 		
		}
	}
	if(leftRegionPage->nRegion == 0 || rightRegionPage->nRegion == 0)
	{  
		printf("in 2\n");
		printf("error in split region page\n");
		printf("old_RP->nRegion=%d\n",old_RP->nRegion);
		printf( "leftRegionPage->nRegion=%d\n",leftRegionPage->nRegion);
		printf("leftRegionPage->Region min=(%f,%f) max=(%f,%f)\n",leftRegionPage->cur_region.min[0],leftRegionPage->cur_region.min[1],leftRegionPage->cur_region.max[0],leftRegionPage->cur_region.max[1]);
		printf( "rightRegionPage->nRegion=%d\n",rightRegionPage->nRegion);
		printf("rightRegionPage->Region min=(%f,%f) max=(%f,%f)\n",rightRegionPage->cur_region.min[0],rightRegionPage->cur_region.min[1],rightRegionPage->cur_region.max[0],rightRegionPage->cur_region.max[1]);
		printf("median=%f\n",median);
		printf("dim=%d\n",dir);
	}
}

double find_splitting_element(const struct kdbPointPage *pointPage)
{

	double *elements;
	double median;
	int i ;
	int index;


	int dir = pointPage->dir;


	if(pointPage->point_num == 0)
		return -1;

	if (pointPage->point_num == 1)
		return pointPage->pList->data[pointPage->dir];

	elements = (double *)malloc( sizeof(double) * pointPage->point_num );


	for(i=0;i<pointPage->point_num;i++)
	{
		elements[i] = pointPage->pList[i].data[dir];
	}

	sort(elements, pointPage->point_num);

	median = elements[ pointPage->point_num / 2 ];

	index = (pointPage->point_num) / 2;
	while( median == elements[pointPage->point_num - 1] && index > 0 )
	{
		median = elements[ index - 1 ];
		index = index - 1;
	}
	if(index == 0 && median == elements[pointPage->point_num - 1])
	{
		printf("can not find median because all points are the same\n");
		exit(1);
	}

	free(elements);

	return median;
}

double find_splitting_element_in_RP(struct kdbRegionPage *regionPage)
{
	struct kdbRegionTuple *rtIns = regionPage->regionSet;
	int i = 0;

	double *elements;
	double median;
	int index;

	int dir = regionPage->dir;

	if(regionPage->nRegion == 0)
		return -1;

	if (regionPage->nRegion == 1)
		return rtIns[0].region.min[dir];


	elements = (double *)malloc( sizeof(double)*(regionPage->nRegion) );

	for(i = 0; i < (regionPage->nRegion); i++, rtIns++) 
	{	
		elements[i] = rtIns->region.max[dir];
	}

	sort(elements, regionPage->nRegion);

	median = elements[ (regionPage->nRegion) / 2 ];

	index = (regionPage->nRegion) / 2;
	while( median == elements[regionPage->nRegion - 1] && index > 0 )
	{
		median = elements[ index - 1 ];
		index = index - 1;
	}
	if(index == 0 && median == elements[regionPage->nRegion - 1])
	{
		printf("Can not find median,so can not split by this dir\n");
		for(int i=0; i < regionPage->nRegion -1 ;i++)
			printf("%f\n",elements[i]);
		return 10.1;
	}

	free(elements);

	return median;	
}


static int is_left_of_xi(const struct kdbRegion *region, int dir, double median)
{
	double ele = region->max[dir];
	if ( ele <= median )
		return 1;
	else 
		return 0;
}

static int is_right_of_xi(const struct kdbRegion *region, int dir, double median)
{
	double ele = region->min[dir];
	if (median <= ele)
		return 1;
	else 
		return 0;	
}

int pointInRegion(const struct kdbRegion *region, const struct kdbPoint *point)
{
	int i; 
	for(i = 0; i < DIMENSION; i++) 
	{
		if(point->data[i] > region->max[i] || point->data[i] < region->min[i])
			return 0;		
	}
	return 1;
}


int pointInPointPage (const struct kdbPointPage *pointPage, const struct kdbPoint *point)                  
{

	int i;
	for(i=0;i < pointPage->point_num;i++)
		if(pointsEqual(&pointPage->pList[i], point))
			return 1; 
	return 0;	
}


int pointsEqual(const struct kdbPoint *firstPoint, const struct kdbPoint *secPoint)
{

	int i;

	for (i = 0; i < DIMENSION; i++) {
		if (firstPoint->data[i] != secPoint->data[i])
			return 0;
	}		
	return 1;
}

void set_page_region(
		const struct kdbRegion oldRegion,
		int dir,
		struct kdbRegion *newRegion,
		double splitElement,
		int side
) 
{
	int i; 
	
	for(i = 0; i < DIMENSION;i++) {
		newRegion->min[i] = oldRegion.min[i];
		newRegion->max[i] = oldRegion.max[i];		
	}

	if (side == LEFT) {	
		newRegion->max[dir] = splitElement;
	} else if(side == RIGHT){
		newRegion->min[dir] = splitElement;
	
	} else {
		printf("side value incorrect");
	
	}
	return;		
}

static void reset_parent_id(struct kdbtree *tree,int child_page_type, int child_page_id, int new_parent_page_id)
{
	struct kdbPointPage *pointPage = NULL;
	struct kdbRegionPage *rp = NULL;

	if (child_page_type == POINT_PAGE) {
		pointPage = &tree->allPointPages[child_page_id];
		if(pointPage == NULL)
		{
		 printf("error in reset_parent_id function find_PointPage_in_global_list\n");
		 kdb_printf(tree,0);
		 exit(1);
		}
		pointPage->parPageID = new_parent_page_id;

	} else if(child_page_type == REGION_PAGE) {
		rp = &tree->allRegionPages[child_page_id];
		if(rp == NULL)
		{
			printf("error in reset_parent_id function find_RegionPage_in_global_list\n");
			kdb_printf(tree,0);
			exit(1);
		}
		rp->parPageID = new_parent_page_id;			
	}

	return;
}

struct kdbPointPage *find_PointPage_contain_queryPoint(struct kdbtree *tree,const struct kdbPoint *queryPoint)
{
	int i;
	for(i=0;i<MAX_POINT_PAGE_NUM;i++)
		if( (tree->allPointPages[i].used == 1) && (pointInRegion( &(tree->allPointPages[i].pp_region), queryPoint) ) )
			return &(tree->allPointPages[i]);

	return NULL; 		
}

static void insert_point(struct kdbPointPage *pointPage, struct kdbPoint *point)
{
	memcpy(&(pointPage->pList[pointPage->point_num]),point,sizeof(struct kdbPoint));
	pointPage->point_num = pointPage->point_num + 1;
}

static int insert_regionTuple(
    struct kdbRegionPage *rp, 
	const struct kdbRegion *region, 
	int page_type, 
	int page_ID
)
{
	int result = 0;

	if (rp->nRegion == MAX_REGION_NUM)
		result = 1;

	rp->regionSet[rp->nRegion].pagetype = page_type;
	rp->regionSet[rp->nRegion].region = *region;
	rp->regionSet[rp->nRegion].pageID = page_ID;

	rp->nRegion = rp->nRegion + 1;

	return result;			
}

void delete_regionTuple(struct kdbRegionPage *rp, int page_type, int page_ID)
{
	struct kdbRegionTuple *list = rp->regionSet;
	int index,i;

	for(index = 0; index < rp->nRegion ; index++) 
	{
		if (list[index].pagetype == page_type && list[index].pageID == page_ID)
			break;
	}

	for(i = index; i < rp->nRegion - 1; i++) 
	{
		memcpy(&list[i],&list[i+1],sizeof(struct kdbRegionTuple));
	}

	memset(&list[rp->nRegion - 1],0,sizeof(struct kdbRegionTuple));

	rp->nRegion--;

	return;
}

int insert_pointPage_in_global_list(struct kdbtree *tree,struct kdbPointPage **pointPage)
{
	int i;
	for(i=0;i < MAX_POINT_PAGE_NUM ;i++)
	{
		if(tree->allPointPages[i].used == 0)
			break;
	}

	tree->allPointPages[i].used = 1;

    tree->allPointPages[i].pageID = i;

	tree->point_page_num++;

	*pointPage = &(tree->allPointPages[i]);

	return INSERT_SUCC;
}

int delete_pointPage_in_global_list(struct kdbtree *tree,struct kdbPointPage *pp)
{
    int i;
	if (tree->point_page_num == 0) {		
		return DELETE_ERROR_NO_ELEMENT;	
	}	
    for(i=0;i<MAX_POINT_PAGE_NUM;i++)
		if( (tree->allPointPages[i].used==1) && (tree->allPointPages[i].pageID == pp->pageID) )
		{
			tree->allPointPages[i].used = 0;
			break;
		}    
	tree->point_page_num = tree->point_page_num - 1;

    return DELETE_SUCC;
}

int insert_regionPage_in_global_list(struct kdbtree *tree,struct kdbRegionPage **rp)
{
	int i;
	for(i=0;i<MAX_REGION_PAGE_NUM;i++)
	{
		if(tree->allRegionPages[i].used == 0)
	      break;
	}

	tree->allRegionPages[i].used = 1;

	tree->allRegionPages[i].pageID = i;

	tree->region_page_num++;

	*rp=&(tree->allRegionPages[i]);
	
	return INSERT_SUCC;	
}

int delete_regionPage_in_global_list(struct kdbtree *tree,struct kdbRegionPage *rp)
{
	int i;
	if (tree->region_page_num == 0) {		
		return DELETE_ERROR_NO_ELEMENT;	
	}
	for(i=0;i < MAX_REGION_PAGE_NUM;i++)
		if( (tree->allRegionPages[i].used == 1) && (tree->allRegionPages[i].pageID == rp->pageID) )	
		{
            tree->allRegionPages[i].used=0;
			break;
		}

	tree->region_page_num = tree->region_page_num - 1;

	return DELETE_SUCC;
}

int kdb_search(struct kdbtree *tree, double x,double y)
{
int i;
int j;
int count=0;

for(j=0;j < MAX_POINT_PAGE_NUM ;j++)
{
	if(tree->allPointPages[j].used == 1)
	{
	 count++;
	 for(i = 0;i<tree->allPointPages[j].point_num;i++)
		if( tree->allPointPages[j].pList[i].data[0] == x  && tree->allPointPages[j].pList[i].data[1] == y )
		{		
			return tree->allPointPages[j].pageID;
		}
	}

	if(count == tree->point_page_num)
		break;
}
return -1;

}

static int regionIntersect(struct kdbRegion *region,struct kdbRegion *queryRegion)
{
 int i=0;
 int intersect[DIMENSION]={0};
 int result;
 result=1;

 for(i=0;i < DIMENSION;i++)
 {
  if(region->min[i] > queryRegion->max[i] || region->max[i] < queryRegion->min[i])
	intersect[i] = 0;
  else 
    intersect[i] = 1;
 }

 for(i=0;i < DIMENSION;i++)
 {
  result = result && intersect[i];
 }
 return result;
}

static int kdb_search_PP(struct kdbtree * tree,struct kdbRegion *queryRegion,int pageID)
{   
	struct kdbPointPage * page;
    //	FILE *fp=fopen("kdb_search_PP.txt","a");
	page = &tree->allPointPages[pageID];

	for(int i=0;i < page->point_num;i++)
	{
		if( pointInRegion(queryRegion, &(page->pList[i])) )
		{
		      // fprintf(fp,"point (%f,%f)\n",page->pList[i].data[0],page->pList[i].data[1]);
		      //  printf("point (%f,%f)\n",page->pList[i].data[0],page->pList[i].data[1]);
		      //  printf("pageid=%d\n",pageID);
		}			
	}

    // 	fclose(fp);
	return 0;
}

static int kdb_search_RP(struct kdbtree * tree,struct kdbRegion *queryRegion,int pageID)
{
	struct kdbRegionPage * rp;
	int i;
	rp = &tree->allRegionPages[pageID];

	for(i=0;i < rp->nRegion;i++)
		if(regionIntersect(&rp->regionSet[i].region,queryRegion) == 1)
		{
			if(rp->regionSet[i].pagetype == POINT_PAGE)
			{
			//	kdb_search_PP(tree,queryRegion,rp->regionSet[i].pageID);
			}

			if(rp->regionSet[i].pagetype == REGION_PAGE)
			{
				kdb_search_RP(tree,queryRegion,rp->regionSet[i].pageID);
			}
		}
    	return 0;
}

int kdb_search_range(struct kdbtree * tree,struct kdbRegion *queryRegion)
{
 if(tree->root_pageID == -1)
	 return -1;

 if(tree->root_page_type == POINT_PAGE)
 {	
   kdb_search_PP(tree,queryRegion,tree->root_pageID);
 }

 if(tree->root_page_type == REGION_PAGE)
 {
   kdb_search_RP(tree,queryRegion,tree->root_pageID);
 }
 return 0;
}

void kdb_printf(struct kdbtree *tree,int first)
{
	int i;
    int j=0;
	int count;

	FILE *fp;
	if(first)
		fp=fopen("out.txt","a");
	else
		fp=fopen("out.txt","w");

	fprintf(fp,">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n");
	fprintf(fp,"根节点ID=%d  ",tree->root_pageID);

	switch(tree->root_page_type)
	{
		case REGION_PAGE:
			fprintf(fp,"根节点类型 REGION PAGE\n");
			break;
		case POINT_PAGE:
			fprintf(fp,"根节点类型 POINT PAGE\n");
			break;
		default:
			fprintf(fp,"根节点类型 UNKNOWN PAGE\n");
			break;
	}	
	fprintf(fp,"region page\n");


	count=0;
	for(i=0;i < MAX_REGION_PAGE_NUM ;i++)
	{
		if(tree->allRegionPages[i].used == 1)
		{
		 count++;
		 fprintf(fp,"pageID=%d",tree->allRegionPages[i].pageID);
		 fprintf(fp,"	isRoot=%d",tree->allRegionPages[i].isRoot);
		 fprintf(fp,"	Parent Page ID=%d\n",tree->allRegionPages[i].parPageID);
		 if(tree->allRegionPages[i].isRoot)
		 {
		   fprintf(fp,"root region turtles num=%d\n",tree->allRegionPages[i].nRegion);
		 }
		 fprintf(fp,"Region Page current region min=(%f,%f) max=(%f,%f)\n",tree->allRegionPages[i].cur_region.min[0],tree->allRegionPages[i].cur_region.min[1],tree->allRegionPages[i].cur_region.max[0],tree->allRegionPages[i].cur_region.max[1]);
		}
	    if(count == tree->region_page_num)
			break;
     }
	fprintf(fp,"region page num=%d\n",tree->region_page_num);

	fprintf(fp,"------------------------------------------------------------------------\n");
	fprintf(fp,"point page\n");

	count=0;
	for(i=0;i < MAX_POINT_PAGE_NUM;i++)
	{
		if(tree->allPointPages[i].used == 1)
		{
		 count++;
		 fprintf(fp,"pageID=%d",tree->allPointPages[i].pageID);

		 fprintf(fp,"   isRoot=%d",tree->allPointPages[i].isRoot);

		 fprintf(fp,"   Parent Page ID=%d",tree->allPointPages[i].parPageID);

		 fprintf(fp,"   Page region min=(%f,%f) max=(%f,%f)\n",tree->allPointPages[i].pp_region.min[0],tree->allPointPages[i].pp_region.min[1],tree->allPointPages[i].pp_region.max[0],tree->allPointPages[i].pp_region.max[1]); 

		 fprintf(fp,"   point_num=%d\n",tree->allPointPages[i].point_num);

	/*	 
		 for(j=0;j<tree->allPointPages[i].point_num;j++)
			fprintf(fp,"point (%f,%f)\n",tree->allPointPages[i].pList[j].data[0],tree->allPointPages[i].pList[j].data[1]);
	*/	 
        }
		if(count == tree->point_page_num)
			break;
	}

	fprintf(fp,"point page num =%d\n",tree->point_page_num);

	fclose(fp);
}

