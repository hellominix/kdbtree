#ifndef _KDBTREE_H_
#define _KDBTREE_H_

#ifdef __cplusplus
extern "C" {
#endif

#define POINT_NUM 10000

#define MAX_REGION_NUM 12
#define MAX_POINT_NUM 21

#define MAX_POINT_PAGE_NUM 1000
#define MAX_REGION_PAGE_NUM 200

#define RECTANGLE_MIN 0
#define RECTANGLE_MAX 1

#define LEFT 0
#define RIGHT 1

#define DIMENSION 2

#define INVALID_PAGE_ID -1

#define REGION_PAGE 10
#define POINT_PAGE 11
#define UNKNOWN_PAGE 12

#define IS_ROOT 1
#define IS_NOT_ROOT 0

#define INSERT_SUCC 0
#define INSERT_ERROR_NULL_PAGE 1
#define INSERT_ALREADY_EXIST 2
#define INSERT_FAILED 3

#define DELETE_SUCC 0
#define DELETE_ERROR_NO_ELEMENT 1
#define DELETE_ERROR_NOT_FOUND_ELEMENT 2
#define DELETE_FAILED 3

#define IS_OVERFLOW 1
#define IS_NOT_OVERFLOW 0

struct kdbRegion 
{
	double min[DIMENSION];
	double max[DIMENSION];             
};

struct kdbPoint 
{
	double data[DIMENSION];
};

struct kdbPointPage 
{
	int used;
	int pageID;					
	int dir;
	int isRoot;						
	int parPageID;
        int point_num;
	struct kdbRegion pp_region;
	struct kdbPoint pList[MAX_POINT_NUM + 1];
};


struct kdbRegionTuple 
{
	struct kdbRegion region;
	int pagetype;
	int pageID;
};

struct kdbRegionPage 
{	
	int used;  
	int pageID;						
	int dir;						
	int isRoot;						
	int parPageID;
	int nRegion;
        struct kdbRegion cur_region;
	struct kdbRegionTuple regionSet[ MAX_REGION_NUM + 1];	
};

struct kdbtree 
{
	int dim;
	int isEmpty;
	int root_page_type;
        int root_pageID;
	int point_page_num;
	int region_page_num;
	struct kdbRegion globalRegion;
	struct kdbPointPage allPointPages[MAX_POINT_PAGE_NUM];
        struct kdbRegionPage allRegionPages[MAX_REGION_PAGE_NUM];
};

struct kdbtree *kdb_create();

void kdb_free(struct kdbtree *tree);

void kdb_clear(struct kdbtree *tree);

int kdb_insert(struct kdbtree *tree, struct kdbPoint *point);

void kdb_printf(struct kdbtree *tree,int first);

int kdb_search(struct kdbtree *tree, double x,double y);

int kdb_search_range(struct kdbtree * tree,struct kdbRegion *queryRegion);

int pointInRegion(const struct kdbRegion *region, const struct kdbPoint *point);

#ifdef __cplusplus
}
#endif
#endif	
