#include <cuda.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "kdbtree.h"
#include "utility.h"

struct kdbPoint *queryPoints[POINT_NUM];

__device__ int regionIntersect(struct kdbRegion *region,struct kdbRegion *queryRegion)
{
	int i=0;
	int intersect[DIMENSION]={0};
	int result;
	result=1;

	for(i=0;i < DIMENSION;i++)
	{
		if(region->min[i] > queryRegion->max[i] || region->max[i] < queryRegion->min[i])
			intersect[i] = 0;
		 else 
			intersect[i] = 1;
	}

	for(i=0;i < DIMENSION;i++)
	{
		result = result && intersect[i];
	}
	return result;
}

__global__ void kdb_range_search_gpu(struct kdbtree * tree,struct kdbRegion *queryRegion,bool intersect[MAX_POINT_PAGE_NUM])
{
	__shared__ bool nextSearch[MAX_REGION_PAGE_NUM];
	__shared__ bool currentSearch[MAX_REGION_PAGE_NUM];
        __shared__ bool hasbitset;
	
	struct kdbRegionPage * rp;
	int i;
	int index;

	currentSearch[tree->root_pageID] = 1;
	hasbitset=1;
  	__syncthreads();

	while(hasbitset)
	{
		hasbitset=0;

		index = threadIdx.x;
		while(index < MAX_REGION_PAGE_NUM)
		{
			nextSearch[index]=0;
			index +=blockDim.x * gridDim.x;
		}

		__syncthreads();
		index =threadIdx.x;

		while(index < MAX_REGION_PAGE_NUM)
		{
			if(currentSearch[index])
			{ 
				rp = &tree->allRegionPages[index];
				for(i=0;i < rp->nRegion;i++)
					if(regionIntersect(&rp->regionSet[i].region,queryRegion) == 1)
					{
						if(rp->regionSet[i].pagetype == POINT_PAGE)
						{  
					//		intersect[ rp->regionSet[i].pageID ] = 1;
						}

						if(rp->regionSet[i].pagetype == REGION_PAGE)
						{	   
							nextSearch[ rp->regionSet[i].pageID ] = 1;
							hasbitset =1;
						}
					}
			}

			index +=blockDim.x * gridDim.x;
		}

		__syncthreads();
		index=threadIdx.x;

		while(index < MAX_REGION_PAGE_NUM)
		{
			currentSearch[index]=nextSearch[index];
			index +=blockDim.x * gridDim.x;
		}
		__syncthreads();
	}

}

int main(int argc, char **argv)
{
	int i;
    int result=-1;

    struct kdbRegion *queryRegion=NULL;
    struct kdbRegion *dev_queryRegion;

	struct kdbtree *tree;
	struct kdbtree *dev_tree;

	bool intersect[MAX_POINT_PAGE_NUM]={0};
	bool *dev_intersect;


	for (i = 0; i < POINT_NUM; i++)
	{
		queryPoints[i] =(struct kdbPoint*) malloc(sizeof(struct kdbPoint));         
		memset(queryPoints[i], 0, sizeof(struct kdbPoint));
		rand_create_point(queryPoints[i],DIMENSION);                  
	}

	queryPoints[9]->data[0]=0.5;
	queryPoints[9]->data[1]=0.5;
	
	
        tree = kdb_create();
	printf("tree storage size%d\n",sizeof(*tree));
        
	clock_t begin,end;
	begin = clock();

	for( i=0; i < POINT_NUM; i++ ) 
	{
		kdb_insert(tree, queryPoints[i]);
	}
	end = clock();
	printf("kdb_insert time=%lf ms\n",double(end-begin)*1000/CLOCKS_PER_SEC);

	kdb_printf(tree,0);

	result=kdb_search(tree,queryPoints[9]->data[0],queryPoints[9]->data[1]);
	printf("found in page %d\n",result);

	queryRegion=(struct kdbRegion *)malloc( sizeof(struct kdbRegion ) );
	queryRegion->min[0]=0.0;
	queryRegion->min[1]=0.0;
	queryRegion->max[0]=1.0;
	queryRegion->max[1]=1.0;
    	
	begin = clock();
	for(int i=0;i<1000;i++)
	kdb_search_range(tree,queryRegion);
	end = clock();
	printf("kdb_search_range_cpu time=%f ms\n",double(end-begin)*1000/CLOCKS_PER_SEC);

	cudaMalloc(&dev_queryRegion,sizeof(struct kdbRegion));
	cudaMalloc(&dev_tree,sizeof(struct kdbtree));
	cudaMalloc(&dev_intersect,sizeof(bool)*MAX_POINT_PAGE_NUM);

	cudaMemcpy(dev_tree,tree,sizeof(struct kdbtree),cudaMemcpyHostToDevice);
	cudaMemcpy(dev_queryRegion,queryRegion,sizeof(struct kdbRegion),cudaMemcpyHostToDevice);
	cudaMemcpy(dev_intersect,intersect,sizeof(bool)*MAX_POINT_PAGE_NUM,cudaMemcpyHostToDevice);
    
	cudaEvent_t start,stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop); 
	cudaEventRecord(start,0);

        kdb_range_search_gpu<<<1,512>>>(dev_tree,dev_queryRegion,dev_intersect);

//	cudaMemcpy(intersect,dev_intersect,sizeof(bool)*MAX_POINT_PAGE_NUM,cudaMemcpyDeviceToHost);
        cudaEventRecord(stop,0);
	cudaEventSynchronize(stop);
	float elapsedTime=0.0;
	cudaEventElapsedTime(&elapsedTime,start,stop);
	printf("kdb_range_search_gpu time= %f ms\n",elapsedTime);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);
/*
	FILE *fp=fopen("kdb_range_search_gpu.txt","w");
	int count=0;
	for(int j=0;j < MAX_POINT_PAGE_NUM;j++)
		if(intersect[j] == 1)
                {
	          for(i=0;i < tree->allPointPages[j].point_num;i++)
	          {
		    if( pointInRegion(queryRegion, &(tree->allPointPages[j].pList[i])) )
		    {
		//	 fprintf(fp,"point (%f,%f)\n",tree->allPointPages[j].pList[i].data[0],tree->allPointPages[j].pList[i].data[1]);
			 count++;
		    }			
	          }
                }

        printf("kdb_range_search_gpu count= %d \n",count);
	count = 0;
        for(int j=0;j < MAX_POINT_PAGE_NUM;j++)
		if(intersect[j]==1)
		{
			fprintf(fp,"%d ",j);
			count++;
		}	
	fprintf(fp,"\ncount=%d\n",count);
        fclose(fp);
*/
	cudaFree(dev_queryRegion);
	cudaFree(dev_tree);
	cudaFree(dev_intersect);

	free(tree);
}
