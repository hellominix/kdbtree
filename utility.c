
#include <stdlib.h>
#include <stdio.h>
#include "utility.h"
#include "kdbtree.h"
#include <sys/time.h>

void rand_create_point(struct kdbPoint *point, int dim)
{
	int i;
	struct timeval t_start;
	gettimeofday(&t_start,NULL);
        srand(t_start.tv_usec);
	for(i = 0; i < dim; i++) 
	{
		point->data[i] =(double) rand() / RAND_MAX;
	}
}

void sort(double *myarray, int len) {
	int i, j;
	double tmp;

	for(j = 0; j < len; j++) {
		for(i = 0; i < len - j -1; i++) {
			if(myarray[i] > myarray[i+1]) {
				tmp = myarray[i];
				myarray[i] = myarray[i+1];
				myarray[i+1] = tmp;			
			}
		}
	}
}
