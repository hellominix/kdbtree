#ifndef _UTILITY_H_
#define _UTILITY_H_

#ifdef __cplusplus
extern "C" {
#endif

void rand_create_point(struct kdbPoint *point, int dim);
void sort(double *myarray, int len);

#ifdef __cplusplus
}
#endif

#endif	
